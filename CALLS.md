## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Thruk Monitoring. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Thruk Monitoring.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Thruk Monitoring. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAlerts(callback)</td>
    <td style="padding:15px">Get Alerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatistics(callback)</td>
    <td style="padding:15px">Get Statistics</td>
    <td style="padding:15px">{base_path}/{version}/checks/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommand(body, callback)</td>
    <td style="padding:15px">Post Command</td>
    <td style="padding:15px">{base_path}/{version}/cmd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommands(callback)</td>
    <td style="padding:15px">Get Commands</td>
    <td style="padding:15px">{base_path}/{version}/commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommand(name, callback)</td>
    <td style="padding:15px">Get Command</td>
    <td style="padding:15px">{base_path}/{version}/commands/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommandConfig(name, callback)</td>
    <td style="padding:15px">Get Command Config</td>
    <td style="padding:15px">{base_path}/{version}/commands/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommandConfig(name, body, callback)</td>
    <td style="padding:15px">Post Command Config</td>
    <td style="padding:15px">{base_path}/{version}/commands/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCommandConfig(name, body, callback)</td>
    <td style="padding:15px">Patch Command Config</td>
    <td style="padding:15px">{base_path}/{version}/commands/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommandConfig(name, callback)</td>
    <td style="padding:15px">Delete Command Cofig</td>
    <td style="padding:15px">{base_path}/{version}/commands/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComments(callback)</td>
    <td style="padding:15px">Get Comments</td>
    <td style="padding:15px">{base_path}/{version}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComment(id, callback)</td>
    <td style="padding:15px">Get Comment</td>
    <td style="padding:15px">{base_path}/{version}/comments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigCheck(callback)</td>
    <td style="padding:15px">Post Config Check</td>
    <td style="padding:15px">{base_path}/{version}/config/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDiff(callback)</td>
    <td style="padding:15px">Get Config Diff</td>
    <td style="padding:15px">{base_path}/{version}/config/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigDiscard(callback)</td>
    <td style="padding:15px">Post Config Discard</td>
    <td style="padding:15px">{base_path}/{version}/config/discard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFiles(callback)</td>
    <td style="padding:15px">Get Config Files</td>
    <td style="padding:15px">{base_path}/{version}/config/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFullObjects(callback)</td>
    <td style="padding:15px">Get Config Full Objects</td>
    <td style="padding:15px">{base_path}/{version}/config/fullobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigObjects(callback)</td>
    <td style="padding:15px">Get Config Objects</td>
    <td style="padding:15px">{base_path}/{version}/config/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigObjects(body, callback)</td>
    <td style="padding:15px">Post Config Objects</td>
    <td style="padding:15px">{base_path}/{version}/config/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigObjects(body, callback)</td>
    <td style="padding:15px">Patch Config Objects</td>
    <td style="padding:15px">{base_path}/{version}/config/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigObject(id, body, callback)</td>
    <td style="padding:15px">Patch Config Object</td>
    <td style="padding:15px">{base_path}/{version}/config/objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigObject(id, body, callback)</td>
    <td style="padding:15px">Post Config Object</td>
    <td style="padding:15px">{base_path}/{version}/config/objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigObject(name, callback)</td>
    <td style="padding:15px">Delete Config Object</td>
    <td style="padding:15px">{base_path}/{version}/config/objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigPrecheck(callback)</td>
    <td style="padding:15px">Get Config Pre Check</td>
    <td style="padding:15px">{base_path}/{version}/config/precheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigReload(callback)</td>
    <td style="padding:15px">Post Config Reload</td>
    <td style="padding:15px">{base_path}/{version}/config/reload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigRevert(callback)</td>
    <td style="padding:15px">Post Config Revert</td>
    <td style="padding:15px">{base_path}/{version}/config/revert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigSave(callback)</td>
    <td style="padding:15px">Post Config Save</td>
    <td style="padding:15px">{base_path}/{version}/config/save?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactGroups(callback)</td>
    <td style="padding:15px">Get Contact Groups</td>
    <td style="padding:15px">{base_path}/{version}/contactgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactGroup(name, callback)</td>
    <td style="padding:15px">Get Contact Group</td>
    <td style="padding:15px">{base_path}/{version}/contactgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactGroupsConfig(name, callback)</td>
    <td style="padding:15px">Get Contact Groups Config</td>
    <td style="padding:15px">{base_path}/{version}/contactgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContactGroupsConfig(name, body, callback)</td>
    <td style="padding:15px">Post Contact Groups Config</td>
    <td style="padding:15px">{base_path}/{version}/contactgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchContactGroupsConfig(name, body, callback)</td>
    <td style="padding:15px">Patch Contact Groups Config</td>
    <td style="padding:15px">{base_path}/{version}/contactgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteContactsGroupConfig(name, callback)</td>
    <td style="padding:15px">Delete Contact Groups Config</td>
    <td style="padding:15px">{base_path}/{version}/contactgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContacts(callback)</td>
    <td style="padding:15px">Get Contacts</td>
    <td style="padding:15px">{base_path}/{version}/contacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContact(name, callback)</td>
    <td style="padding:15px">Get Contact</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactConfig(name, callback)</td>
    <td style="padding:15px">Get Contact Config</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContactConfig(name, body, callback)</td>
    <td style="padding:15px">Post Contact Config</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchContactConfig(name, body, callback)</td>
    <td style="padding:15px">Patch Contact Config</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteContactConfig(name, callback)</td>
    <td style="padding:15px">Delete Contact Config</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDowntimes(callback)</td>
    <td style="padding:15px">Get Downtimes</td>
    <td style="padding:15px">{base_path}/{version}/downtimes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDowntime(id, callback)</td>
    <td style="padding:15px">Get Downtime</td>
    <td style="padding:15px">{base_path}/{version}/downtimes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostgroups(callback)</td>
    <td style="padding:15px">Get Hostgroups</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostgroup(name, callback)</td>
    <td style="padding:15px">Get Hostgroup</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostgroupConfig(name, callback)</td>
    <td style="padding:15px">Get Hostgroup Config</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHostgroupConfig(name, body, callback)</td>
    <td style="padding:15px">Post Hostgroup Config</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchHostgroupConfig(name, body, callback)</td>
    <td style="padding:15px">Patch Hostgroup Config</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHostgroupConfig(name, callback)</td>
    <td style="padding:15px">Delete Hostgroup Config</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostgroupOutages(name, callback)</td>
    <td style="padding:15px">Get Hostgroup Outages</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups/{pathv1}/outages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostgroupStats(name, callback)</td>
    <td style="padding:15px">Get Hostgroup Stats</td>
    <td style="padding:15px">{base_path}/{version}/hostgroups/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHosts(callback)</td>
    <td style="padding:15px">Get Hosts</td>
    <td style="padding:15px">{base_path}/{version}/hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHost(name, callback)</td>
    <td style="padding:15px">Get Host</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostAlerts(name, callback)</td>
    <td style="padding:15px">Get Host Alerts</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostCommandLine(name, callback)</td>
    <td style="padding:15px">Get Host Command Line</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/commandline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostConfig(name, callback)</td>
    <td style="padding:15px">Get Host Config</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHostConfig(name, body, callback)</td>
    <td style="padding:15px">Post Host Config</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchHostConfig(name, body, callback)</td>
    <td style="padding:15px">Patch Host Config</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHostConfig(name, callback)</td>
    <td style="padding:15px">Delete Host Config</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostNotifications(name, callback)</td>
    <td style="padding:15px">Get Host Notifications</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostOutages(name, callback)</td>
    <td style="padding:15px">Get Host Outages</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/outages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostServices(name, callback)</td>
    <td style="padding:15px">Get Host Services</td>
    <td style="padding:15px">{base_path}/{version}/hosts/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOutages(callback)</td>
    <td style="padding:15px">Get Outages</td>
    <td style="padding:15px">{base_path}/{version}/hosts/outages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostStats(callback)</td>
    <td style="padding:15px">Get Host Stats</td>
    <td style="padding:15px">{base_path}/{version}/hosts/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostTotals(callback)</td>
    <td style="padding:15px">Get Host Totals</td>
    <td style="padding:15px">{base_path}/{version}/hosts/totals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndex(callback)</td>
    <td style="padding:15px">Get Index</td>
    <td style="padding:15px">{base_path}/{version}/index?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLMDSites(callback)</td>
    <td style="padding:15px">Get LMD Sites</td>
    <td style="padding:15px">{base_path}/{version}/lmd/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogs(callback)</td>
    <td style="padding:15px">Get Logs</td>
    <td style="padding:15px">{base_path}/{version}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotifications(callback)</td>
    <td style="padding:15px">Get Notifications</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProcessInfo(callback)</td>
    <td style="padding:15px">Get Process Info</td>
    <td style="padding:15px">{base_path}/{version}/processinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProcessInfoStats(callback)</td>
    <td style="padding:15px">Get Process Info Stats</td>
    <td style="padding:15px">{base_path}/{version}/processinfo/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceGroups(callback)</td>
    <td style="padding:15px">Get Service Groups</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceGroup(name, callback)</td>
    <td style="padding:15px">Get Service Group</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceGroupConfig(name, callback)</td>
    <td style="padding:15px">Get Service Group Config</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServiceGroupConfig(name, body, callback)</td>
    <td style="padding:15px">Post Service Group Config</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchServiceGroupConfig(name, body, callback)</td>
    <td style="padding:15px">Patch Service Group Config</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceGroupConfig(name, callback)</td>
    <td style="padding:15px">Delete Service Group Config</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceGroupOutages(name, callback)</td>
    <td style="padding:15px">Get Service Group Outages</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups/{pathv1}/outages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceGroupStats(name, callback)</td>
    <td style="padding:15px">Get Service Group Stats</td>
    <td style="padding:15px">{base_path}/{version}/servicegroups/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(callback)</td>
    <td style="padding:15px">Get Services</td>
    <td style="padding:15px">{base_path}/{version}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostService(host, service, callback)</td>
    <td style="padding:15px">Get Host Service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostServiceCommandLine(host, service, callback)</td>
    <td style="padding:15px">Get Host Service Command Line</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/{pathv2}/commandline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostServiceConfig(host, service, callback)</td>
    <td style="padding:15px">Get Host Service Config</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHostServiceConfig(host, service, body, callback)</td>
    <td style="padding:15px">Post Host Service Config</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchHostServiceConfig(host, service, body, callback)</td>
    <td style="padding:15px">Patch Host Service Config</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHostServiceConfig(host, service, callback)</td>
    <td style="padding:15px">Delete Host Service Config</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostServiceOutages(host, service, callback)</td>
    <td style="padding:15px">Get Host Service Outages</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/{pathv2}/outages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesOutages(callback)</td>
    <td style="padding:15px">Get Services Outages</td>
    <td style="padding:15px">{base_path}/{version}/services/outages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesStats(callback)</td>
    <td style="padding:15px">Get Services Stats</td>
    <td style="padding:15px">{base_path}/{version}/services/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesTotals(callback)</td>
    <td style="padding:15px">Get Services Totals</td>
    <td style="padding:15px">{base_path}/{version}/services/totals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSites(callback)</td>
    <td style="padding:15px">Get Sites</td>
    <td style="padding:15px">{base_path}/{version}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThruk(callback)</td>
    <td style="padding:15px">getThruk</td>
    <td style="padding:15px">{base_path}/{version}/thruk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThrukWhoami(callback)</td>
    <td style="padding:15px">Get Thruk whoami</td>
    <td style="padding:15px">{base_path}/{version}/thruk/whoami?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimePeriods(callback)</td>
    <td style="padding:15px">Get Time Periods</td>
    <td style="padding:15px">{base_path}/{version}/timeperiods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimePeriod(name, callback)</td>
    <td style="padding:15px">Get Time Period</td>
    <td style="padding:15px">{base_path}/{version}/timeperiods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimePeriodConfig(name, callback)</td>
    <td style="padding:15px">Get Time Period Config</td>
    <td style="padding:15px">{base_path}/{version}/timeperiods/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTimePeriodConfig(name, body, callback)</td>
    <td style="padding:15px">Post Time Period Config</td>
    <td style="padding:15px">{base_path}/{version}/timeperiods/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTimePeriodConfig(name, body, callback)</td>
    <td style="padding:15px">Patch Time Period Config</td>
    <td style="padding:15px">{base_path}/{version}/timeperiods/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTimePeriodConfig(name, callback)</td>
    <td style="padding:15px">Delete Time Period Config</td>
    <td style="padding:15px">{base_path}/{version}/timeperiods/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
