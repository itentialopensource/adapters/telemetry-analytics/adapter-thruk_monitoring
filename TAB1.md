# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Thruk_monitoring System. The API that was used to build the adapter for Thruk_monitoring is usually available in the report directory of this adapter. The adapter utilizes the Thruk_monitoring API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Thruk Monitoring adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Thruk Monitoring to offer a Monitoring, Events and Analytics. 

With this adapter you have the ability to perform operations with Thruk Monitoring such as:

- Sites
- Hosts
- Services

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
