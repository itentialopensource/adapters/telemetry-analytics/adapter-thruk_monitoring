# Thruk Monitoring

Vendor: Thruk
Homepage: https://www.thruk.org/

Product: Monitoring
Product Page: https://www.thruk.org/

## Introduction
We classify Thruk Monitoring into the Service Assurance domain as Thruk Monitoring provides information on Events and Monitoring of Networks. 

"Thruk is free of charge and completely open source software."
"While beeing fully flexible and extendable, either by plugins or themes, Thruk is still easy too use." 

## Why Integrate
The Thruk Monitoring adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Thruk Monitoring to offer a Monitoring, Events and Analytics. 

With this adapter you have the ability to perform operations with Thruk Monitoring such as:

- Sites
- Hosts
- Services

## Additional Product Documentation
The [Thruk REST API](https://www.thruk.org/documentation/rest.html)

